# syntax=docker/dockerfile:1
ARG MYSQL_DEV_VERSION
FROM mysql:$MYSQL_DEV_VERSION

COPY dump-local.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/dump-local.sh
ENTRYPOINT ["/usr/local/bin/dump-local.sh"]

CMD [ "--default-authentication-plugin=mysql_native_password", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci", "--skip-character-set-client-handshake" ]