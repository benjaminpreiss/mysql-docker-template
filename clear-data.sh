#!/bin/bash

# Execute this script to remove local mysql data.

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
sudo rm -rf $dir/volume/*