#!/bin/bash

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

${dir}/dump-remote.sh
${dir}/copy-dump.sh
${dir}/clear-data.sh