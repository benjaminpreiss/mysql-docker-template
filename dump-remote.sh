#!/bin/bash

# Execute this script to dump the remote mysql database to a local backup file
# Run after dump-local.sh
# Requirements: a) SSH_USER must have root rights, b) MYSQL_USER must be root and can not have a password.

now=$(date '+%Y_%m_%d-%H:%M:%S')
dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"

ssh ${SERVER_PROD_SSH_USER}@${SERVER_PROD_IP} mysqldump \
    -h ${MYSQL_PROD_DB_HOST} \
    --port=${MYSQL_PROD_DB_PORT} \
    -u"${MYSQL_PROD_DB_USER}" -p"${MYSQL_PROD_DB_PASS}" ${MYSQL_PROD_DATABASE} --no-tablespaces > ${dir}/dumps-remote/${now}.sql
