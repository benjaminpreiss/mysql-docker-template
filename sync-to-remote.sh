#!/bin/bash

# Run after executing script "dump-remote.sh".
# Imports last (alphabetical order) database dump to production (remote) database.
# Requirements: a) SSH_USER must have root rights, b) MYSQL_USER must be root and can not have a password.

last=$(ls -1 ./dumps-local | tail -n 1)
dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

ssh ${SERVER_PROD_SSH_USER}@${SERVER_PROD_IP} mysql -u"${MYSQL_PROD_DB_USER}" -p"${MYSQL_PROD_DB_PASS}" ${MYSQL_PROD_DATABASE} < ./dumps-local/${last}