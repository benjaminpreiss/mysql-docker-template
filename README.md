# MySQL Docker database

A repository for a mysql database run via docker and some helper scripts

## Getting started

- Clone the repository
- Make all scripts executable: `chmod u+x *.sh`
- Make sure that all needed env variables for the bash scripts are defined

## When used as a submodule

### Docker Compose

Copy this into the docker-compose.yml file of the main repo:

```yml
mysql:
    build:
        context: ./mysql
        dockerfile: mysql-dev.Dockerfile
        args: 
            MYSQL_DEV_VERSION: ${MYSQL_DEV_VERSION}
    container_name: ${PROJECT_NAME}-mysql
    ports:
        - '${MYSQL_DEV_DB_PORT}:3306'
    volumes:
        - ./mysql/volume:/var/lib/mysql
        - ./mysql/initialization/:/docker-entrypoint-initdb.d/
        - ./mysql/dumps-local:/mnt/dumps-docker-host
    environment:
        MYSQL_DATABASE: ${MYSQL_DEV_DATABASE}
        MYSQL_USER: ${MYSQL_DEV_DB_USER}
        MYSQL_PASSWORD: ${MYSQL_DEV_DB_PASS}
        MYSQL_RANDOM_ROOT_PASSWORD: '1'
    networks: 
        - app-network
adminer:
    image: adminer
    ports:
        - ${ADMINER_DEV_PORT}:8080
    networks: 
        - app-network
```

### Env Variables

Make sure that all env variables/secrets are defined inside the main repos config-dev.sh and config-prod.sh. Fill in the values to the following vars:

PROJECT_NAME=...
MYSQL_PROD_VERSION=...

...dev:

```bash
#!/bin/bash

# ...

# General

echo PROJECT_NAME='*******' # Your project name here

# Adminer

echo ADMINER_DEV_PORT='8090'

# Mysql

echo MYSQL_DEV_DATABASE='dev_db_name'
echo MYSQL_DEV_DB_USER='dev_db_user'
echo MYSQL_DEV_DB_HOST='localhost'
echo MYSQL_DEV_DB_PORT='3306'
echo MYSQL_DEV_DB_PASS='dev_db_pass'
echo MYSQL_DEV_VERSION=$MYSQL_PROD_VERSION

# ...

```

...prod:

```bash
#!/bin/bash

# ...

# Mysql

echo MYSQL_PROD_VERSION='8' # Your production mysql version here

# ...

```

## Cloning remote database to local

In case of the local database docker container already running:

- Dump local database with helper script *dump-local.sh*

... And then continue with the next steps. Otherwise, start with the following steps:

- Dump remote database with helper script *dump-remote.sh*
- Copy remote database dump with helper script *copy-dump.sh*
