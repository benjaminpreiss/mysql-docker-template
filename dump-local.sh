#!/bin/sh

local_dump() {
    now=$(date '+%Y_%m_%d-%H:%M:%S')
    mysqldump \
        -h ${MYSQL_DEV_DB_HOST} \
        --port=${MYSQL_DEV_DB_PORT} \
        -u"${MYSQL_DEV_DB_USER}" -p"${MYSQL_DEV_DB_PASS}" ${MYSQL_DEV_DATABASE} --no-tablespaces > /mnt/dumps-docker-host/${now}.sql
}

trap 'local_dump' HUP INT QUIT TERM

exec /usr/local/bin/docker-entrypoint.sh "$@" &
wait $!
